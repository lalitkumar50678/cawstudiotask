/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Provider,useDispatch, connect } from 'react-redux';
import store from './src/redux/reducer/index';


import RootNavigator from './src/navigations/RootNavigator';

const App: () => React$Node = () => {

  // const dispatch = useDispatch();
  useEffect(()=>{
    console.log('App render methos calling.....')  
  
    console.disableYellowBox = true;
  },[])

  
  return (
    <Provider store={store}>
     <RootNavigator />
    </Provider>
  );
};


export default App;
