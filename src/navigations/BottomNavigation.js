import React from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import CreatePostModel from './models/CreatePostModel';
import DashboardScreen from '../screens/home/Dashbard/Dashboard';
import Library from '../screens/home/Library/Library';
import Messages from '../screens/home/Services/Services';
import Services from '../screens/home/Services/Services';
import images from '../utilities/images';
import Colors from '../utilities/Colors';
import styles from './styles';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const BottomNavigation = () => {
  const dashBoardIcon = (label, isSelected) => (
    <View style={styles.centerAlign}>
      <Image source={images.feed}  style={{tintColor: isSelected ? Colors.btnEnableColor : Colors.gray}} />
      <Text style={[styles.bottomTxtStyle,{color: isSelected ? Colors.btnEnableColor : Colors.gray}]}>
        {label}
      </Text>
    </View>
  );

  const otherIcon = (label, src, isSelect) => (
    <View style={styles.centerAlign}>
    <Image
      source={src}
      style={[
        styles.iconStyle,
        {tintColor: isSelect ? Colors.btnEnableColor : Colors.gray},
      ]}
      tintColor={isSelect ? Colors.btnEnableColor : Colors.gray}
    />
    <Text style={[styles.bottomTxtStyle,{color: isSelect ? Colors.btnEnableColor : Colors.gray}]}>
        {label}
      </Text>
    </View>
  );

  const dummyComponent = () =>{return null};

  

  const CustomBarView = ({state, descriptors, navigation}) => {
    const focusedOptions = descriptors[state.routes[state.index].key].options;

    if (focusedOptions.tabBarVisible === false) {
      return null;
    }

    return (
      <View style={styles.bottomViewStyle}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityStates={isFocused ? ['selected'] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{flex: 1}}>
              {index == 0
                ? dashBoardIcon(label, isFocused)
                : index == 1
                ? otherIcon(label,images.library ,isFocused)
                : index == 2
                ?  (<CreatePostModel isSelected={isFocused} />) // circularButton(label, isFocused)
                :  index == 3 ? otherIcon(label,images.messages, isFocused) : 
                otherIcon(label,images.services, isFocused)}
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

 
  

  return (
    <Tab.Navigator
      tabBar={(props) => <CustomBarView {...props} />}
      >
      <Tab.Screen name="Feed" component={DashboardScreen} />
      <Tab.Screen name="Library" component={Library} />
      <Tab.Screen name="Add" component={dummyComponent} 
       options={{
        tabBarButton: () => (<CreatePostModel />),
      }} />
      <Tab.Screen name="Messages" component={Messages} />
      <Tab.Screen name="Services" component={Services} />
    </Tab.Navigator>
  );
};
export default BottomNavigation;
