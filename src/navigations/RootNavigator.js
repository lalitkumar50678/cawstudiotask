import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {connect} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import BottomNavigation from './BottomNavigation';
import {isUserLogin} from '../utilities/AppData';
import FeedDetails from '../screens/home/FeedDetail/index';
import SplashScreen from '../screens/splash/index';
import StackNavigator from '../navigations/StackNavigator';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
const RootNavigator = () => {
  const isUserLogin = useSelector(state => state.LoginReducer.isUserLogin);

  useEffect(() => {}, []);

  const dashBoardStack = () => (
    <Stack.Navigator headerMode={'none'} headerShown={false}>
      <Stack.Screen name="BottomNavigation" component={BottomNavigation} />
      <Stack.Screen name="FeedDetails" component={FeedDetails} />
    </Stack.Navigator>
  );

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'} >
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="AuthScreen" component={StackNavigator} />
        <Stack.Screen name="HomeScreen" component={dashBoardStack} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = state => {
  // console.log('map state to props  -> ', state);
  return state;
};

export default connect(mapStateToProps, {})(RootNavigator);
