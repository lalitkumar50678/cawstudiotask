import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/auth/login/Login';
const Stack = createStackNavigator();

const AuthStack=()=> {
  
    console.log('Auth Stack is render ')
    return (
      <Stack.Navigator 
      headerMode={'none'}  headerShown={false}>
        <Stack.Screen name="Login" component={Login} />
        
      </Stack.Navigator>
    );
  }

export default AuthStack;