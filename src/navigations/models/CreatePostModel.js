import React, {useState} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';

import Modal from 'react-native-modal';
import Colors from '../../utilities/Colors';
import images from '../../utilities/images';
import mainStyles from '../styles';

const dataList = [
  {
    id: '1',
    title: 'Create a post',
    subTitle: 'Share your thoughts with the community',
    src: images.edit,
  },
  {
    id: '2',
    title: 'Ask a Question',
    subTitle: 'Any doubts? As the community',
    src: images.question,
  },
  {
    id: '3',
    title: 'Start a Poll',
    subTitle: 'Need the opinion of the many? ',
    src: images.poll,
  },
  {
    id: '4',
    title: 'Organise an Event',
    subTitle: 'Start a meet with people to share your joys',
    src: images.poll,
  },
];
const CreatePostModel = (props) => {
  const [modalVisible, setModalVisible] = useState(false);

  const circularButton = () => (
    <TouchableOpacity
      style={mainStyles.centerAlign}
      onPress={() => {
        setModalVisible(true);
      }}>
      <View
        style={[
          mainStyles.circularBtnStyle,
          {
            backgroundColor: props.isSelected
              ? Colors.btnEnableColor
              : Colors.gray,
          },
        ]}>
        <Image
          source={images.plus}
          style={[
            mainStyles.plusIconStyle,
            {
              tintColor: props.isSelected
                ? Colors.white
                : Colors.btnEnableColor,
            },
          ]}
          tintColor={props.isSelected ? Colors.white : Colors.btnEnableColor}
        />
      </View>
    </TouchableOpacity>
  );

  return (
    <>
      {circularButton()}
      <View style={styles.container}>
        <Modal
          backdropOpacity={0.3}
          isVisible={modalVisible}
          onBackdropPress={() => setModalVisible(false)}
          style={styles.contentView}>
          <>
            <View style={styles.content}>
              {dataList.map((item) => (
                <>
                  <View style={styles.itemRootStyle}>
                    <View style={styles.circularCenter}>
                      <Image source={item.src} />
                    </View>
                    <View style={styles.titleViewStyle}>
                      <Text style={styles.titleStyle}>{item.title}</Text>
                      <Text style={styles.subtitleSty}>{item.subTitle}</Text>
                    </View>
                    <View style={styles.arrowStyle}>
                      <Image source={images.arrowLeft} />
                    </View>
                  </View>
                  <View style={styles.bottomLineStyle} />
                </>
              ))}
            </View>
            <TouchableOpacity
              style={styles.whiteBorder}
              onPress={() => {
                setModalVisible(false);
              }}>
              <Image source={images.cross} />
            </TouchableOpacity>
          </>
        </Modal>
      </View>
    </>
  );
};

export default CreatePostModel;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemRootStyle: {
    height: 60,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    
  },
  circularCenter: {
    height: 40,
    width: 40,
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.lightBlue,
  },
  titleViewStyle: {
    marginStart: 5,
  },
  titleStyle: {
    fontSize: 12,
    fontFamily: 'Inter-Regular',
    color: Colors.btnEnableColor,
  },
  subtitleSty: {
    fontSize: 10,
    fontFamily: 'Inter-Regular',
    color: Colors.txtGrayColor,
  },
  content: {
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 5,
    justifyContent: 'center',
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12,
  },
  contentView: {
    justifyContent: 'flex-end',
    marginHorizontal: 10,
  },
  buttonStyle: {
    flex: 1,
    backgroundColor: Colors.primaryDarkColor,
    borderRadius: 100,
  },
  bottomLineStyle: {
    height: 0.5,
    backgroundColor: Colors.bottomLineColor,
    width: '100%',
  },
  arrowStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
  whiteBorder: {
    height: 37,
    width: 37,
    borderRadius: 37,
    marginTop: 10,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
