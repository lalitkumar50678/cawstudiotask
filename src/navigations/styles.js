import React,{useState} from 'react';
import {StyleSheet} from 'react-native';
import commonStyle from '../utilities/CommonStyle';
import Colors from '../utilities/Colors';
import {getWidth} from '../utilities/dimentions';
let isSelected = false;

const styles =
    
  StyleSheet.create({
     
    rootViewStyle: {
      height: 30,
      width: 28,
      borderRadius: 5,
     
      borderWidth: 1.5,
    },
    circleStyle: {
        width: 5,
        height: 5,
        borderRadius: 10,
        
    },
    lineStyle:{
        width: 14,
        height: 2,
        
        marginStart: 1,
        borderRadius: 2
    },
    smallBox:{
        width: '80%',
        height: 5,
        
        marginStart: 2,
        borderRadius: 2,
        marginTop: 1,
    },
    largeBox: {
        width: '80%',
        height: 7,
        
        marginStart: 2,
        borderRadius: 2,
        marginTop: 1,
    },
    iconStyle:{
        height: 25,
        width: 25,
    },
    centerAlign:{justifyContent: 'center', alignItems: 'center', paddingVertical: 1},
    bottomTxtStyle:{
        fontFamily: 'Inter-Bold',
        fontSize: 10
    },
    circularBtnStyle:{
        height: 40,
        width: 40,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    plusIconStyle:{
        height: 15,
        width: 15,
    },
    bottomViewStyle: {flexDirection: 'row', backgroundColor: Colors.white, elevation: 5},
  });

export default styles;
