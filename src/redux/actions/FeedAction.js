import React from 'react';
import { USER_LIKE_ACTION, GET_USER_LOGIN, 
  GET_USER_FEED_BY_TYPE, SELECT_FEED_DETAILS} from './Type';
import {setUserLogin, isUserLogin } from '../../utilities/AppData';



export const setUserLike = (feedId) =>dispatch =>  {
  console.log('setUserLike calling ', feedId, );
  dispatch( {
        type: USER_LIKE_ACTION,
        payload: {
          feedId,
          likeType: 'like'
        }
      })
      setUserLike(feedId);
    //   setUserLogin(isLogin,()=>{
        
    // })
  }



  export const getUserFeedByType= (type) => dispatch => {
    dispatch( {
      type: GET_USER_FEED_BY_TYPE,
      payload: type
    })
  }


  export const selectFeedDetail= (feedId) => dispatch=> {
    dispatch( {
      type: SELECT_FEED_DETAILS,
      payload: feedId
    })
  }
