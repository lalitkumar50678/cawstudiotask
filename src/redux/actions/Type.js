export const SET_USER_LOGIN = 'set_user_login';
export const GET_USER_LOGIN = 'get_user_login';
export const USER_LIKE_ACTION = 'USER_LIKE_ACTION';
export const GET_USER_FEED_BY_TYPE = 'GET_USER_FEED_BY_TYPE';
export const SELECT_FEED_DETAILS = 'SELECT_FEED_DETAILS';