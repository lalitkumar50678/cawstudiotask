import {USER_LIKE_ACTION, GET_USER_FEED_BY_TYPE, SELECT_FEED_DETAILS} from '../actions/Type';
import postData from './PostData';
import commentList from './commentList';
import images from '../../utilities/images';

const initialState = {
  isUserLogin: false,
  userObj: {
    userId: '001',
    userName: 'Lalit kumar',
    userImg: 'https://randomuser.me/api/portraits/men/16.jpg',
  },
  allFeeds: [...postData],
  selectFeed: null,
  commentList,
};

const FeedReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LIKE_ACTION:
      console.log('action.feedId --> ', action);
      return {
        ...state,
        allFeeds: state.allFeeds.map((item) =>
          item.id === action.payload.feedId
            ? {
                ...item,
                likeCount:  item.postLikedUser.filter(
                  (item) => item.userId === state.userObj.userId,
                ).length > 0 ? item.likeCount - 1: item.likeCount + 1,
                postLikedUser:
                  item.postLikedUser.filter(
                    (item) => item.userId === state.userObj.userId,
                  ).length > 0
                    ? [
                        ...item.postLikedUser.filter(
                          (item) => item.userId !== state.userObj.userId,
                        ),
                      ]
                    : [...item.postLikedUser, {...state.userObj}],
              }
            : item,
        ),
      };

    case GET_USER_FEED_BY_TYPE:
      console.log('GET_USER_FEED_BY_TYPE --> ', action);
      if (action.payload.type === 'all') {
        return {
          ...state,
          allFeeds: state.allFeeds,
        };
      } else {
        return {
          ...state,
          allFeeds: state.allFeeds.filter(
            (item) => item.type === action.payload.type,
          ),
        };
      }
     case SELECT_FEED_DETAILS:
       console.log('SELECT_FEED_DETAILS -->  ', action.payload);
       const index = state.allFeeds.findIndex(item => item.id == action.payload)
       console.log('SELECT_FEED_DETAILS -->  ', index);
       if(index !== -1)
       return{
         ...state,
         selectFeed:  state.allFeeds[index],
         
       } 

    default:
      return state;
  }
};

export default FeedReducer;
