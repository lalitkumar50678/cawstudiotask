import { SET_USER_LOGIN, GET_USER_LOGIN} from '../actions/Type';


const initialState = {
    isUserLogin: false,
    
  }
  
  const LoginReducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_USER_LOGIN:
       return {
           ...state,
           isUserLogin : action.payload,
       }
      case GET_USER_LOGIN: 
      return {
          ...state,
          isUserLogin :   action.payload,
      }
      default:
        return state
    }
  }
  
  export default LoginReducer