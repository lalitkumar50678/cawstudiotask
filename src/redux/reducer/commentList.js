import images from '../../utilities/images';

const commentList= [
    {
        userId: '101',
        userName: 'username1275',
        userImg: 'https://randomuser.me/api/portraits/men/99.jpg',
        time: '1min',
        designation: 'Influencer',
        designationImg: images.influencer,
        isTopAnswer: true,
        experience: '5+ yrs Champion',
        commentType: 'text',
        comment: 'Not sure about rights. Looks like its a matter of concern that our schools don’t take it seriously such matters and treat it so lightly like its a fault of the student.',
        likeCount: 2,
        dislikeCount: 2,
        replyCount: 2,
    },
    {
        userId: '102',
        userName: 'username1275',
        userImg: 'https://randomuser.me/api/portraits/men/99.jpg',
        time: '1min',
        designation: 'Diagnosed Recently',
        designationImg: null,
        experience: '',
        isTopAnswer: false,
        commentType: 'textImage',
        imagesList: [images.images,images.images, images.images, images.images, images.images],
        comment: 'Not sure about rights. Looks like its a matter of concern that our schools don’t take it seriously such matters and treat it so lightly like its a fault of the student.',
        likeCount: 2,
        dislikeCount: 2,
        replyCount: 0,
    },
    {
        userId: '001',
        userName: 'Lalit kumar',
        userImg: 'https://randomuser.me/api/portraits/men/16.jpg',
        time: '1min',
        designation: 'Diagnosed Recently',
        designationImg: null,
        experience: '',
        isTopAnswer: false,
        commentType: 'text',
        comment: 'Not sure about rights. Looks like its a matter of concern that our schools don’t take it seriously such matters and treat it so lightly like its a fault of the student.',
        likeCount: 1,
        dislikeCount: 0,
        replyCount: 0,
    }
]

export default commentList;
