import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import LoginReducer from './LoginReducer';
import FeedReducer from './FeedReducer';

const combine = combineReducers({
    LoginReducer : LoginReducer,
    FeedReducer : FeedReducer,
});

const store = createStore(combine,applyMiddleware(thunk));
export default store;