import React,{useState} from 'react';
import { useDispatch, useSelector, connect } from "react-redux";
import {View, StyleSheet, Image, TextInput, TouchableOpacity} from 'react-native';
import Toast from 'react-native-simple-toast';
import commonStyle from '../../../utilities/CommonStyle';
import StatusBar from '../../../utilities/StatusBar';
import images from '../../../utilities/images';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import Strings from '../../../utilities/Strings';
import styles from './styles';
import {setUserLoginAction} from '../../../redux/actions/LoginAction'
import {setUserLogin} from '../../../utilities/AppData';

const LoginScreen = (props) => {

    const [phone,setPhone] = useState('')
    
    const dispatch = useDispatch();

  const flagRender = () => (
    <View style={styles.flagStyle}>
      <View style={styles.firstColor} />
      <View style={styles.secondColor}>
        <View style={styles.circleStyle}>
          <View style={styles.centerDot} />
        </View>
      </View>
      <View style={styles.thirdColorStyle} />
    </View>
  );

  
  const sentOtp=()=>{
      console.log('sentOtp calling...')
    if(phone.length == 10){
        console.log(' props.navigation ---> ',  props.navigation.navigate);
        setUserLogin(true,()=>{
          dispatch(setUserLoginAction(true));
          props.navigation.navigate("HomeScreen")
        })
        
    }else{
        Toast.show(Strings.Login.correctPhoneNumber)
        console.log('sentOtp less than 10 calling...')
    }
  }

  const renderButton=()=>(
      <TouchableOpacity style={[styles.buttonStyle,{backgroundColor: phone.length ==10 ? Colors.btnEnableColor : Colors.buttonBgColor }]} onPress={sentOtp}>
            <TextView style={[styles.buttonTxtStyle,{color: phone.length ==10 ? Colors.white : Colors.textColor }]}>{Strings.Login.sendOTP}</TextView>
      </TouchableOpacity>
  )

  return (
    <View style={commonStyle.container}>
      <StatusBar />
      <View style={styles.topViewStyle}>
        <Image source={images.signInLogo} style={styles.logoStyle} />
        <TextView style={styles.headerTitle}>
          {Strings.Login.welcomeTxt}
        </TextView>
      </View>
      <View style={styles.phoneInputStyle}>
        {flagRender()}
        <Image
          source={images.arrowdown}
          resizeMode={'contain'}
          style={styles.arrowDownStyle}
        />
        <View style={styles.inputPhoneViewStyle}>
          <TextInput
            style={styles.inputFlag}
            placeholder={Strings.Login.enterTxt}
            placeholderTextColor={Colors.textColor}
            keyboardType={'phone-pad'}
            onChangeText={txt => setPhone(txt)}
            maxLength={10}
          />
          <Image
            source={images.Shape}
            resizeMode={'contain'}
            style={styles.arrowDownStyle}
          />
        </View>
      </View>
      <TextView style={styles.securityTxtStyle}>
        {Strings.Login.securityTxt}
      </TextView>
      {renderButton()}
    </View>
  );
};


export default  LoginScreen;


