import React from 'react';
import {StyleSheet} from 'react-native';
import commonStyle from '../../../utilities/CommonStyle';
import Colors from '../../../utilities/Colors';
import {getWidth} from '../../../utilities/dimentions';


const styles = StyleSheet.create({
    logoStyle: {
      marginTop: 50,
      borderRadius: 5,
    },
    topViewStyle: {
      marginTop: 150,
      marginStart: 15,
    },
    rootStyle: {
      ...commonStyle.container,
      backgroundColor: Colors.lightWhite,
    },
    headerTitle: {
      fontSize: 30,
      fontFamily: 'Inter-Bold',
    },
    phoneInputStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      marginStart: 15,
    },
    flagStyle: {
      height: 20,
      width: 40,
      borderRadius: 10,
      justifyContent: 'flex-start',
    },
    firstColor: {
      backgroundColor: Colors.flagFirstColor,
      height: 10,
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
    },
    secondColor: {
      backgroundColor: Colors.flagSecondColor,
      alignItems: 'center',
      justifyContent: 'center',
      height: 10,
    },
    circleStyle: {
      height: 10,
      width: 10,
      borderRadius: 10,
      borderWidth: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    centerDot: {
      backgroundColor: Colors.flagCircleColor,
      height: 2,
      width: 2,
      borderRadius: 4,
    },
    thirdColorStyle: {
      height: 10,
      backgroundColor: Colors.flagThirdColor,
      borderBottomLeftRadius: 5,
      borderBottomRightRadius: 5,
    },
    arrowDownStyle: {
      alignContent: 'center',
      padding: 2,
      width: 20,
      marginStart: 2,
    },
    inputFlag: {
      width: getWidth(65),
      fontSize: 14,
      height: 40,
      fontFamily: 'Inter-Regular',
      color: Colors.textColor,
    },
    inputPhoneViewStyle: {
      borderBottomColor: Colors.phoneBottomColor,
      flexDirection: 'row',
      alignItems: 'center',
      borderBottomWidth: 1,
      justifyContent: 'flex-start',
      paddingEnd: 5
    },
    securityTxtStyle: {
      fontFamily: 'Inter-Regular',
      color: Colors.textColor,
      fontSize: 12,
      paddingStart: 15,
      paddingEnd: 15,
      marginTop: 15,
    },
    buttonStyle:{
        height: 45,
        width: 100,
        backgroundColor: Colors.buttonBgColor,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        alignSelf: 'center'
    },
    buttonTxtStyle:{
      color : Colors.textColor,
      fontFamily: 'Inter-Bold',
      fontSize: 15
    }
  });

  export default styles;