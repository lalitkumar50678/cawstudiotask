import React from 'react';
import {View, Image, StyleSheet, FlatList} from 'react-native';
import images from '../../../utilities/images';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import Strings from '../../../utilities/Strings';
import styles from './styles';

const ArticleList = (props) => {
  renderItem = ({item}) => (
   <View style={styles.articleRootViewSty}>
    <View style={styles.rowViewSty}>
        <Image source={{uri: item.articleImg}} style={styles.articleUserImgSty} />
        <TextView style={styles.articlePostNameSty} >
        {item.articleTitle}
      </TextView>

    </View>

    <TextView style={styles.articleDescriptionSty} >
        {item.articleDes}
      </TextView>
    <View style={styles.articleBorderSty} />
    <View style={styles.articleBottomViewSty}>
    <TextView style={styles.moreArticleTxtStyle} >
        {Strings.DashBoard.readArticle.toUpperCase()}
      </TextView>
      <Image source={images.arrowLeft} style={{tintColor: Colors.btnEnableColor}}  />
    </View>
  </View>); 


  renderSeparator = () => (
    <View style={styles.articleSeparator} />
  );

  return (
    <View style={styles.articleViewSty}>
      <TextView style={styles.articleTxtHeaderSty} maxLength={150}>
        {props.item.title}
      </TextView>
      <FlatList
        style={styles.articleListSty}
        data={props.item.ListData}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
        horizontal
      />
    </View>
  );
};

export default ArticleList;
