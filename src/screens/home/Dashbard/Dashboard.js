import React, {useState, useRef} from 'react';
import {View, FlatList, Animated} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import commonStyle from '../../../utilities/CommonStyle';
import StatusBar from '../../../utilities/StatusBar';
import images from '../../../utilities/images';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import Strings from '../../../utilities/Strings';
import styles from './styles';

import HeaderComponent from './HeaderComponent';
import ListItem from './ListItem';
import SharePost from './sharePost/SharePost';
import ReportPost from './ReportPost/index';


const DashBoardScreen = (props) => {
  const [isShareDialogVisible, setShareDialogVisible] = useState(false);
  const [isReportDialogVisible, setReportDialogVisible] = useState(false);
  const allFeeds = useSelector((state) => state.FeedReducer.allFeeds);

  const heightView = useRef(new Animated.Value(0)).current;


  shareBtnClick = (item) => {
    console.log('item clickl ', item);
    setShareDialogVisible(!isShareDialogVisible);
  };

 
  onReportBtnClick = (item) => {
    setReportDialogVisible(true);
  };

  renderItem = ({item}) => (
    <ListItem
      item={item}
      shareBtnClick={shareBtnClick}
      onReportClick={(item) => onReportBtnClick(item)}
      navigation={props.navigation}
    />
  );

  // renderSeparator=()=>(

  // )

  sharePostVisibility = (isVisi) => {
    setShareDialogVisible(isVisi);
  };

  return (
    <View style={[commonStyle.container, {}]}>
      <StatusBar />
      {console.log('heightView --> ', heightView)}
      <HeaderComponent animatedHeight={heightView} />
      <FlatList
        data={allFeeds}
        renderItem={renderItem}
        //ItemSeparatorComponent={renderSeparator}
        keyExtractor={(item) => item.id}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {y: heightView},
              },
            },
          ]
        )}
      />

      <SharePost
        visibility={isShareDialogVisible}
        handleVisibility={(isVis) => sharePostVisibility(isVis)}
      />
      <ReportPost
        visibility={isReportDialogVisible}
        handleVisibility={(isVis) => setReportDialogVisible(isVis)}
      />
    </View>
  );
};

export default DashBoardScreen;
