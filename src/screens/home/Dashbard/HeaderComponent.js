import React,{useMemo} from 'react';
import {Animated,View, Image, TextInput, FlatList, TouchableOpacity} from 'react-native';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import styles from './styles';
import Strings from '../../../utilities/Strings';
import images from '../../../utilities/images';

const dataList = [
  {title: 'All Posts', isSelect: false},
  {
    title: 'News',
    isSelect: false,
  },
  {
    title: 'Diet',
    isSelect: false,
  },
  {
    title: 'Lifestyle',
    isSelect: false,
  },
  {
    title: 'Symptoms',
    isSelect: false,
  },
];

const HeaderComponent = (props) => {
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        borderRadius: 15,
        backgroundColor: item.isSelect ? Colors.lightBlue : Colors.white,
        borderWidth: 0.5,
        borderColor: Colors.searchBox
      }}>
      <TextView style={{fontFamily: 'proxima_nova', fontSize: 12, padding: 8, color: Colors.btnEnableColor}}>{item.title}</TextView>
    </TouchableOpacity>
  );

  const HorizontalList = () => (
    <FlatList data={dataList} 
    renderItem={renderItem}
    style={{marginTop: 5, marginBottom: 5}}

    horizontal 
        ItemSeparatorComponent={()=> <View style={{marginStart: 5}} />}
    />
  );

 
 

  return (useMemo(()=>(
    <Animated.View style={[styles.headerRoot,{
       },
    ]}>
      <Animated.View style={[styles.headerVericalView,{
        transform: [{
      translateY: props.animatedHeight.interpolate({
      inputRange: [
        0,
        300],
      outputRange: [0,-60],
      extrapolate: "clamp"
    }),
    }],
    height: props.animatedHeight.interpolate({
      inputRange: [
        0,
        300],
      outputRange: [50,0],
      extrapolate: "clamp"
    })
      }]}>
        <Animated.View>
          <TextView style={styles.communityTxtStyle}>
            {Strings.DashBoard.community.toUpperCase()}
          </TextView>
          <Animated.View
            style={styles.rowViewSty}>
            <TextView
              style={styles.allDCommunityTxt}>
              {Strings.DashBoard.allCommunities}
            </TextView>
            <Image
              source={images.arrowdown}
              style={styles.arrowDownSty}
              resizeMode={'contain'}
            />
          </Animated.View>
        </Animated.View>
        <Animated.View style={styles.rightImgViewSty}>
          <Animated.Image
            source={images.dummyImg}
            style={styles.rightDummyImgSty}
            resizeMode={'contain'}
          />
        </Animated.View>
      </Animated.View>
      <Animated.View style={styles.searchBoxRootSty}>
        <Animated.View
          style={styles.searchBoxViewSty}>
          <TextInput
            style={styles.searchInputTxtSty}
            placeholder={Strings.DashBoard.searchPlace}
          />

          <Image source={images.search} />
        </Animated.View>
        <Animated.View
          style={styles.notificationIconViewSty}>
          <Image source={images.notification} />
        </Animated.View>
      </Animated.View>
      {HorizontalList()}
    </Animated.View>
  ),[]));
};

export default HeaderComponent;
