import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {useDispatch} from 'react-redux';
import images from '../../../utilities/images';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import Question from './ListItems/Question';
import {selectFeedDetail} from '../../../redux/actions/FeedAction';
import Strings from '../../../utilities/Strings';
import styles from './styles';
import EventItem from './ListItems/EventItem';
import LifeStyle from './ListItems/LifeStyle';
import ListItemFooter from './ListItemFooter';
import ArticleList from './ArticleList';
import PollItem from './ListItems/PollItem';

const ListItem = (props) => {

    const dispatcher = useDispatch();

    goToDetailsPage=()=>{
        dispatcher(selectFeedDetail(props.item.id))
         props.navigation.navigate("FeedDetails",{
             item : props.item,
         }) 
    }



  return (
    <>
     {!props.isFromDetailPage && <View style={{height: 5, backgroundColor: Colors.gray}} />}  
      {props.item.type === 'AddList' ? (
        <ArticleList item={props.item} />
      ) : (
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 5,
              paddingHorizontal: 10,
            }}>
            <TextView
              style={{
                fontFamily: 'Inter-Bold',
                fontSize: 10,
                color: Colors.gray,
              }}>
              {props.item.postContainType}
            </TextView>
            <TextView
              style={{
                fontFamily: 'Inter-Thin',
                fontSize: 10,
                color: Colors.userNameColor,
              }}>
              {props.item.time}
            </TextView>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{height: 40, width: 40, borderRadius: 20}}
                source={
                  props.item.userImg === 'anonymous'
                    ? images.anonymous
                    : {uri: props.item.userImg}
                }
              />
              <View style={{marginStart: 5}}>
                <View style={styles.rowViewSty}>
                  <TextView
                    style={{
                      fontSize: 12,
                      fontFamily: 'Inter-Bold',
                      color: Colors.userNameColor,
                    }}>
                    {props.item.userName}
                  </TextView>
                  <TextView
                    style={{
                      fontSize: 12,
                      fontFamily: 'Inter-Thin',
                      marginStart: 2,
                    }}>
                    {props.item.postType}
                  </TextView>
                </View>

                <View style={styles.rowViewSty}>
                  {props.item.type === 'lifeStyle' && (
                    <Image source={images.expert} />
                  )}
                  <TextView
                    style={{
                      fontSize: 10,
                      fontFamily: 'Inter-Regular',
                      color: Colors.btnEnableColor,
                    }}>
                    {props.item.patientType.toUpperCase()}
                  </TextView>
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={() => props.onReportClick(props.item)}
              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
              <Image source={images.option} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={goToDetailsPage}>
            {props.item.type === 'question' || props.item.type === 'post' ? (
              <Question item={props.item} />
            ) : props.item.type === 'event' ? (
              <EventItem item={props.item} />
            ) : props.item.type === 'lifeStyle' ? (
              <LifeStyle item={props.item} />
            ) : props.item.type === 'Poll' ? (
              <PollItem item={props.item} />
            ) : null}
          </TouchableOpacity>
          <ListItemFooter
            item={props.item}
            shareBtnClick={(item) => props.shareBtnClick(item)}
          />
        </View>
      )}
    </>
  );
};

export default ListItem;
