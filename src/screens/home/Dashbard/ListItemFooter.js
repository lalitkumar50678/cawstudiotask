import React, {useState, useRef, useEffect} from 'react';
import {View, Image, TouchableOpacity, Animated} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Popover from 'react-native-popover-view';
import images from '../../../utilities/images';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import Question from './ListItems/Question';
import Strings from '../../../utilities/Strings';
import styles from './styles';
import {setUserLike} from '../../../redux/actions/FeedAction';

const animatedEmojis = [
  {
    title: 'Like',
    src: images.thumb,
  },
  {
    title: 'Haha',
    src: images.haha,
  },
  {
    title: 'Sad',
    src: images.sad,
  },
];

const ListItemFooter = (props) => {
  const [popoverVisible, setPopoverVisible] = useState(false);

  const [isCurrentUserLinked, setCurrentUserLiked] = useState(false);
  const tranlateAnim = useRef(new Animated.Value(5)).current;
  const currentUser = useSelector((state) => state.FeedReducer.userObj);
  const updateFeed = useSelector((state) => state.FeedReducer.allFeeds);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('updateFeed calling ',currentUser);
    const condition =
      props.item.postLikedUser.filter(
        (item) => item.userId === currentUser.userId,
      ).length > 0;
    console.log('condition calling ', condition);
    setCurrentUserLiked(condition)
  }, [updateFeed]);


  
  likeButton = useRef(null);

  useEffect(() => {
    Animated.timing(tranlateAnim, {
      // fromValue: 10,
      toValue: 2,
      duration: 200,
      useNativeDriver: true,
    }).start();
   
  }, [popoverVisible]);

  _renderEmoji = (item) => (
    <TouchableOpacity
      onPress={() => {
        setPopoverVisible(false);
        dispatch(setUserLike(props.item.id));
    
      }}
      style={[
        styles.footerEmojiStyle,
        {
          transform: [
            {
              translateY: tranlateAnim,
            },
          ],
        },
      ]}>
      <Image
        style={styles.emojiImgStyle}
        source={item.src}
        resizeMode={'contain'}
      />
      <TextView style={styles.emojiTxtSty}>{item.title}</TextView>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <View style={styles.lineStyle} />
      <View>
        <View
          style={styles.likedUserViewSty}>
          <View style={styles.rowViewSty}>
            {props.item.postLikedUser.map(
              (item, index) =>
                index < 3 && (
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      borderRadius: 10,
                      position: index == 0 ? 'relative' : 'absolute',
                      start: index * 12,
                      zIndex: props.item.postLikedUser.length - index,
                    }}
                    source={{uri: item.userImg}}
                  />
                ),
            )}
          </View>

          <TextView
            style={styles.likedUserTxtStyle}>
            {isCurrentUserLinked
              ? `${Strings.DashBoard.youAnd}${props.item.likedPerson}`
              : `${props.item.likedPerson}`}
          </TextView>
        </View>
      </View>
      <View style={styles.lineStyle} />
      <View style={[styles.rowStyle, styles.reactionRootViewStyle]}>
        <TouchableOpacity
          ref={likeButton}
          style={styles.reactionStyle}
          hitSlop={styles.hipSlopSty}
          onPress={() => setPopoverVisible(true)}>
          <Image
            source={
              props.item.postLikedUser.filter(
                (item) => item.userId === currentUser.userId,
              ) > 0
                ? images.thumb
                : props.item.type === 'question'
                ? images.hand
                : images.likedImg
            }
          />
          <TextView>{props.item.likeCount}</TextView>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.reactionStyle}
          hitSlop={styles.hipSlopSty}>
          <Image source={images.commentIng} />
          <TextView>{props.item.commentCount}</TextView>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.reactionStyle}
          hitSlop={styles.hipSlopSty}>
          <Image source={images.tags} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.reactionStyle}
          onPress={() => props.shareBtnClick(props.item)}
          hitSlop={styles.hipSlopSty}>
          <Image source={images.share} />
        </TouchableOpacity>
      </View>
      <Popover
        isVisible={popoverVisible}
        from={likeButton}
        placement="top"
        
        onRequestClose={() => setPopoverVisible(false)}
        backgroundStyle={{backgroundColor: '#0005'}}
        popoverStyle={styles.popOverViewStyle}
        arrowStyle={{
          backgroundColor: 'transparent',
        }}>
        <View style={{flexDirection: 'row'}}>
          {animatedEmojis.map(_renderEmoji)}
        </View>
      </Popover>
    </View>
  );
};

export default ListItemFooter;
