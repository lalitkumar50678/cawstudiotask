import React from 'react';
import {View ,Image, StyleSheet, TouchableOpacity} from 'react-native';
import images from '../../../../utilities/images';
import Colors from '../../../../utilities/Colors';
import TextView from '../../../../utilities/TextView';
import Strings from '../../../../utilities/Strings';
import styles from './styles';
import {getWidth} from '../../../../utilities/dimentions';


const EventItem =(props)=>{


    return(
        <View style={{}}>
            
                <View style={styles.viewHoriPadding} >
                <TextView style={styles.eventTitleStyle}>{props.item.event.eventTitle.toUpperCase()}</TextView>        
                <TextView style={styles.eventTimeSty} maxLength={100}>{props.item.event.eventTime.toUpperCase()}</TextView>        

                </View>
            
            <View style={styles.viewRowSty}>
                <Image style={[images.location, {tintColor: Colors.gray}]} source={images.location} resizeMode={'contain'} tintColor={Colors.gray} />
                <TextView style={styles.addressTxtSty}>{props.item.address}</TextView>
            </View>
            <View style={{backgroundColor: Colors.lightBlue, overflow: 'hidden', flex: 1,paddingHorizontal: 15, paddingVertical: 5}}>
                    <View style={{justifyContent: 'space-between', paddingVertical: 5, flexDirection: 'row',}}>
                        <View >
                        <TextView style={styles.eventQuestionTxtSty}>{props.item.event.eventQuestion}</TextView>
                      <View style={styles.viewRowSty}>
                            <Image source={images.people} />
                            <TextView style={styles.eventAnswerTxtSty}>{props.item.event.eventAnswer}</TextView>
                      </View>    
                        </View>
                        <View style={styles.viewRowSty}>
                            {props.item.event.eventQuestionBtn.map((item)=> (
                                <TouchableOpacity
                                    style={{
                                        borderRadius: 15,
                                        backgroundColor: Colors.lightBlue,
                                        borderWidth: 0.5,
                                        borderColor: Colors.searchBox,
                                        width: 50,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginStart: 5
                                    }}>
                                    <TextView style={{fontFamily: 'proxima_nova', fontSize: 12, padding: 8, color: Colors.btnEnableColor}}>{item}</TextView>
                                    </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                      
            </View>
        </View>
    )
    
}
export default EventItem;
