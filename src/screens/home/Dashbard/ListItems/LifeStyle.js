import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import images from '../../../../utilities/images';
import Colors from '../../../../utilities/Colors';
import TextView from '../../../../utilities/TextView';
import Strings from '../../../../utilities/Strings';
import styles from './styles';

const LifeStyle = (props) => {
  return (
    <View style={{}}>
      <View>
        <TextView style={styles.lifestyleHeaderTxtSty}>
          {props.item.title}
        </TextView>
        <TextView style={styles.lifestyleLinkTxtSty} maxLength={100}>
          {props.item.linkTxt}
        </TextView>
        <Image source={images.running}  />
      </View>

      <View style={styles.addressViewSty}>
        <Image source={images.location} resizeMode={'contain'} />
        <TextView style={styles.addressTxtSty}>{props.item.address}</TextView>
      </View>
    </View>
  );
};

export default LifeStyle;
