import React from 'react';
import {View, Image} from 'react-native';
import images from '../../../../utilities/images';
import Colors from '../../../../utilities/Colors';
import TextView from '../../../../utilities/TextView';
import Strings from '../../../../utilities/Strings';
import styles from './styles';
import {getWidth} from '../../../../utilities/dimentions';

const PollItem = (props) => {
  return (
    <View style={{paddingHorizontal: 10}}>
      <View>
        <TextView style={styles.quesTitleSty}>
          {props.item.pollData.title}
        </TextView>
        {props.item.pollData.pollOptions.map((poll) => (
          <View style={{marginVertical: 5, borderRadius: 5}}>
            <View
              style={{
                height: 30,
                width: getWidth(parseInt(poll.percentage)),
                backgroundColor: Colors.buttonBgColor,
              }}></View>
            <View
              style={{
                height: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                position: 'absolute',
                alignItems: 'center',
                flex: 1,
              }}>
              <TextView style={{fontFamily: 'Inter-Thin',paddingHorizontal: 5, fontSize: 12, width: '80%'}}>
                {poll.title}
              </TextView>
              <TextView style={{fontFamily: 'Inter-Regular', fontSize: 12}}>
                {poll.percentage}
              </TextView>
            </View>
          </View>
        ))}
      </View>
      <TextView style={styles.pollStatus}>
          {props.item.pollData.pollStatus}
        </TextView>
    </View>
  );
};

export default PollItem;
