import React from 'react';
import {View ,Image, StyleSheet} from 'react-native';
import images from '../../../../utilities/images';
import Colors from '../../../../utilities/Colors';
import TextView from '../../../../utilities/TextView';
import Strings from '../../../../utilities/Strings';
import styles from './styles';



const Question=(props)=>{


    return(
        <View style={{paddingHorizontal: 10}}>
            {props.item.type == 'question' ? (
                <View >
                 {!!props.item.title && (<TextView style={styles.quesTitleSty}>{props.item.title}</TextView>)}        
                <TextView style={styles.queDescriptionSty} maxLength={100}>{props.item.description}</TextView>        
                </View>
            ) :(
                <TextView style={styles.queDescriptionSty} maxLength={150}>{props.item.description}</TextView>        
            )}
            <View style={styles.viewRowSty}>
                <Image source={images.location} resizeMode={'contain'} />
                <TextView style={styles.addressTxtSty}>{props.item.address}</TextView>
            </View>
        </View>
    )
}

export default Question;