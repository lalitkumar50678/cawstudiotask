import React from 'react';
import {View, StyleSheet} from 'react-native';
import Colors from '../../../../utilities/Colors';

const styles = StyleSheet.create({
  quesTitleSty: {fontFamily: 'Inter-Bold', fontSize: 14, color: Colors.userNameColor},
  queDescriptionSty: {fontFamily: 'Inter-Regular', fontSize: 12},
  viewRowSty: {flexDirection: 'row'},
  addressTxtSty: {
    fontFamily: 'Inter-Regular',
    fontSize: 10,
    color: Colors.btnEnableColor,
    marginStart: 5
  },
  eventTitleStyle:{
    fontFamily: 'Inter-Bold', 
    fontSize: 16
  },
  eventTimeSty:{
    fontFamily: 'Inter-Regular', 
    fontSize: 10
  },
  eventQuestionTxtSty:{
    fontFamily: 'Inter-Bold',
    fontSize: 14,
  },
  eventAnswerTxtSty:{
    fontFamily: 'Inter-Regular',
    fontSize: 10,
    marginStart: 5
  },
  viewHoriPadding:{paddingHorizontal: 15},
  lifestyleHeaderTxtSty:{
    fontFamily: 'Inter-Regular',
    fontSize: 12,
    paddingHorizontal: 15,
  },
  lifestyleLinkTxtSty:{
    fontFamily: 'Inter-Regular',
    fontSize: 12,
    color: Colors.blue,
    paddingHorizontal: 15,
  },
  addressViewSty:{
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  pollStatus: {
    fontFamily: 'Inter-Regular',
    fontSize: 12,
    color: Colors.phoneBottomColor,
    paddingVertical: 5
  }


});

export default styles;
