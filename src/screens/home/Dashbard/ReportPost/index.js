import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Modal from 'react-native-modal';
import images from '../../../../utilities/images';
import TextView from '../../../../utilities/TextView';
import styles from './styles';
import {getWidth,getHeight } from '../../../../utilities/dimentions';
import Strings from '../../../../utilities/Strings';

const dataList = [
  {
    id: '0021',
    title: 'Hide <Post type>',
    subTitle: 'See fewer posts like this',
    src: images.eyes,
  },
  {
    id: '0022',
    title: 'Unfollow <username>',
    subTitle: 'See fewer posts like this',
    src: images.unfollow,
  },
  {
    id: '0023',
    title: 'Report <Post type>',
    subTitle: 'See fewer posts like this',
    src: images.report,
  },
  {
    id: '0024',
    title: 'Copy <post type> link',
    subTitle: 'See fewer posts like this',
    src: images.attachment,
  }
];
const ReportPost = (props) => {
  const [modalVisible, setModalVisible] = useState(false);


  return (
    <>
      
      <View style={styles.container}>
        <Modal
          backdropOpacity={0.3}
          isVisible={props.visibility}
          onBackdropPress={() => props.handleVisibility(false)}
          onBackButtonPress={() => props.handleVisibility(false)}
          style={styles.contentView}
          scrollOffset={5}
          deviceWidth={getWidth(100)}
          deviceHeight={getHeight(100)}>

          <>
            <View style={styles.content}>
            
              <View style={{alignItems: 'center' , marginTop: 5 }}>
              <Image source={images.trigger}/>

              <TextView style={styles.sharePostTxtStyle}>{Strings.DashBoard.sharePost}</TextView>
              </View>
              {dataList.map((item) => (
                <>
                  <TouchableOpacity style={styles.itemRootStyle} onPress={() => props.handleVisibility(false)}>
                    
                      <Image source={item.src} />
                    
                    <View style={styles.titleViewStyle}>
                      <TextView style={styles.titleStyle}>{item.title}</TextView>
                      <TextView style={styles.subTitleStyle}>{item.title}</TextView>
                    </View>
                  </TouchableOpacity>
                  <View style={styles.bottomLineStyle} />
                </>
              ))}
            </View>
            
          </>
        </Modal>
      </View>
    </>
  );
};

export default ReportPost;

