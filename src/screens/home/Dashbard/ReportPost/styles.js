import {StyleSheet} from 'react-native';
import Colors from '../../../../utilities/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  itemRootStyle: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    flexDirection: 'row',
    
    width: '100%',
  },
  
  titleViewStyle: {
    marginStart: 5,
  },
  titleStyle: {
    fontSize: 12,
    fontFamily: 'Inter-Regular',
    color: Colors.userNameColor,
  },
  subTitleStyle: {
    fontSize: 8,
    fontFamily: 'Inter-Regular',
    color: Colors.phoneBottomColor,
  },
  sharePostTxtStyle:{
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    color: Colors.userNameColor,
    marginTop: 5
  },

  content: {
    backgroundColor: Colors.white,

    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },

  contentView: {
    justifyContent: 'flex-end',
    width: '100%',
    marginHorizontal: 1,
    
  },
  bottomLineStyle: {
    height: 0.5,
    backgroundColor: Colors.bottomLineColor,
    width: '80%',
    alignSelf: 'center',
  },
  arrowStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
});

export default styles;
