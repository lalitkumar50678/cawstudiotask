import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Modal from 'react-native-modal';
import images from '../../../../utilities/images';
import TextView from '../../../../utilities/TextView';
import styles from './styles';
import {getWidth,getHeight } from '../../../../utilities/dimentions';
import Strings from '../../../../utilities/Strings';

const dataList = [
  {
    id: '0010',
    title: 'Send as a private message',
    src: images.sendMsg,
  },
  {
    id: '0011',
    title: 'Share as a post',
    src: images.sharePost,
  },
  {
    id: '0013',
    title: '',
    src: null,
  },
  {
    id: '0014',
    title: 'Share on Facebook',
    src: images.facebookIcon,
  },
  {
    id: '0015',
    title: 'Send via WhatsApp',
    src: images.whatsApp,
  },
];
const SharePostModel = (props) => {
  const [modalVisible, setModalVisible] = useState(false);


  return (
    <>
      
      <View style={styles.container}>
        <Modal
          backdropOpacity={0.3}
          isVisible={props.visibility}
          onBackdropPress={() => props.handleVisibility(false)}
          onBackButtonPress={() => props.handleVisibility(false)}
          style={styles.contentView}
          scrollOffset={5}
          deviceWidth={getWidth(100)}
          deviceHeight={getHeight(100)}>

          <>
            <View style={styles.content}>
            
              <View style={{alignItems: 'center' , marginTop: 5 }}>
              <Image source={images.trigger}/>

              <TextView style={styles.sharePostTxtStyle}>{Strings.DashBoard.sharePost}</TextView>
              </View>
              {dataList.map((item) => (
                <>
                  <TouchableOpacity style={styles.itemRootStyle} onPress={() => props.handleVisibility(false)}>
                    
                      <Image source={item.src} />
                    
                    <View style={styles.titleViewStyle}>
                      <TextView style={styles.titleStyle}>{item.title}</TextView>
                    </View>
                  </TouchableOpacity>
                </>
              ))}
            </View>
            
          </>
        </Modal>
      </View>
    </>
  );
};

export default SharePostModel;

