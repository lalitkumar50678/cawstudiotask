import {StyleSheet} from 'react-native';
import Colors from '../../../../utilities/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  itemRootStyle: {
    height: 45,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  
  titleViewStyle: {
    marginStart: 5,
  },
  titleStyle: {
    fontSize: 12,
    fontFamily: 'Inter-Regular',
    color: Colors.btnEnableColor,
  },
  sharePostTxtStyle:{
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    color: Colors.userNameColor,
    marginTop: 5
  },

  content: {
    backgroundColor: Colors.white,

    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },

  contentView: {
    justifyContent: 'flex-end',
    width: '100%',
    marginHorizontal: 1,
    
  },
  bottomLineStyle: {
    height: 0.5,
    backgroundColor: Colors.bottomLineColor,
    width: '100%',
  },
  arrowStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
});

export default styles;
