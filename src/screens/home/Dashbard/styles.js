import {StyleSheet} from 'react-native';
import Colors from '../../../utilities/Colors';

const styles = StyleSheet.create({
  headerRoot: {
    paddingHorizontal: 15,
    marginTop: 15,
    
  },
  headerVericalView: {flexDirection: 'row', height: 50, width: '100%'},
  communityTxtStyle:{fontFamily: 'Inter-Regular', color: Colors.gray},
  rowViewSty: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 2,
  },
  allDCommunityTxt:{
    fontFamily: 'Inter-Bold',
    color: Colors.btnEnableColor,
  },
  arrowDownSty:{
    tintColor: Colors.btnEnableColor,
    marginStart: 2,
    width: 10,
    height: 15,
  },
  rightImgViewSty:{flex: 1, alignItems: 'flex-end', marginTop: 10},
  rightDummyImgSty: {height: 35, 
    width: 35, 
    borderRadius: 35
},
 searchBoxRootSty: {flexDirection: 'row'},
searchBoxViewSty: {
    flexDirection: 'row',
    borderRadius: 5,
    height: 40,
    width: '80%',
    borderWidth: 0.5,
    borderColor: Colors.searchBox,
    alignItems: 'center',
  },
  searchInputTxtSty: {
    fontFamily: 'Inter-Regular',
    width: '90%',
  },
  notificationIconViewSty: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginEnd: 15,
  },
  container:{
    //flex: 1,
    height: 80
  },
  likedUserImgSty:{
    height: 15,
    width: 15,
    borderRadius: 15, 
    position: 'absolute',
    left: 5
  },
  rowStyle:{
    flexDirection: 'row',
  },
  reactionStyle:{
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  articleViewSty:{
    backgroundColor: Colors.gray,
    paddingStart: 10,

  },
  articleTxtHeaderSty:{
    color: Colors.userNameColor,
    fontFamily: 'Inter-Bold'
  },
  articlePostNameSty:{
    color: Colors.userNameColor,
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    marginStart: 5,
  },
  articleDescriptionSty:{
    color: Colors.userNameColor,
    fontFamily: 'Inter-Regular',
    fontSize: 12,
  },
  articleRootViewSty: {
    backgroundColor: Colors.white, 
    borderRadius: 5, 
    paddingHorizontal: 10, 
    width: 200
  },
  articleUserImgSty:{
    width: 25, 
    height: 25, 
    borderRadius: 25
  },
  articleBorderSty:{
    height: 0.5,
    backgroundColor : Colors.bottomLineColor, 
    width: '100%'
  },
  articleBottomViewSty:{
    height: 30, 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    flexDirection: 'row', 
    paddingVertical: 10
  },
  moreArticleTxtStyle:{
    fontFamily: 'Inter-Bold',
    fontSize: 12,
    color: Colors.btnEnableColor
  },
  articleSeparator: {
    width: 10, 
    backgroundColor: Colors.gray
  },
  articleListSty: {
    marginTop: 15, 
    marginEnd: 10, 
    marginBottom: 15
  },
  lineStyle:{height: 0.5, backgroundColor: Colors.buttonBgColor},
  reactionRootViewStyle: {paddingVertical: 10, alignItems: 'center', justifyContent: 'center'},
  hipSlopSty: {top: 15, bottom: 15, right: 15,left: 15},
  footerEmojiStyle: {
    flexDirection: 'row', alignItems: 'center',marginHorizontal: 5
  },
  emojiImgStyle: {
    height: 28,
    aspectRatio: 1,
    marginVertical: 8,
    borderRadius: 14,
    marginStart: 8,
    // overflow: "visible",
  },
  emojiTxtSty: {fontFamily: 'Inter-Regular'},
  likedUserViewSty:{
    flexDirection: 'row',
    paddingVertical: 5,
    paddingStart: 10,
    alignItems: 'center',
  },
  likedUserTxtStyle:{fontFamily: 'Inter-Thin', fontSize: 12, marginStart: 30},
  popOverViewStyle:{
    height: 44,
    width: 220,
    borderRadius: 22,
    borderColor:  Colors.white,
    marginStart: 15,
    marginBottom: 15,
    elevation: 5,
    
  },
});

export default styles;
