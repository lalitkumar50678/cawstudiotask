import React from 'react';
import {View, Image, TouchableOpacity, TouchableHighlight} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ImageSlider from 'react-native-image-slider';
import TextView from '../../../utilities/TextView';
import Colors from '../../../utilities/Colors';
import images from '../../../utilities/images';
import strings from '../../../utilities/Strings';
import styles from './styles';

const CommentItemRow = (props) => {
  const currentUserId = useSelector(
    (state) => state.FeedReducer.userObj.userId,
  );

  React.useEffect(() => {
    console.log('currentUserId  --> ', currentUserId);
  }, []);


  return (
    <View>
      {currentUserId !== props.item.userId ? (
        <View style={styles.commentRootStyle}>
          <Image
            source={{uri: props.item.userImg}}
            style={styles.commentCircularImg}
          />
          <View style={styles.commentViewStyle}>
            <View style={styles.commentListNaveViewSty}>
              <View>
                <View style={[styles.rowStyle,{alignItems: 'center'}]}>
                  <TextView style={styles.userNameTxtStyl}>
                    {props.item.userName}
                  </TextView>
                  <TextView style={styles.userNameTimeStyle}>
                    {props.item.time}
                  </TextView>
                </View>
                <View style={styles.rowStyle}>
                  <View style={styles.designation}>
                    <Image source={props.item.designationImg} />
                    <TextView style={[styles.designationTxtStyle,{fontFamily: props.item.isTopAnswer ? 'Inter-Bold' : 'Inter-Thin'}]}>
                      {props.item.designation}
                    </TextView>
                  </View>
                  <View style={styles.lineStyle} />
                  <TextView style={styles.experienceTxtStyle}>
                    {props.item.experience}
                  </TextView>
                </View>
              </View>
              <TouchableOpacity hitSlop={{top: 10, bottom: 10, }} style={{marginTop: 10}}>
                <Image source={images.option} />
              </TouchableOpacity>
            </View>
            <TextView
              style={{
                fontFamily: 'Inter-Regular',
                fontSize: 12,
                paddingVertical: 5,
                flex: 1,
              }}>
              {props.item.comment}
            </TextView>
            {props.item.commentType === 'textImage' && (
              <View
                style={{
                  width: '80%',
                  alignSelf: 'center',
                  padding: 1,
                  borderRadius: 10,
                }}>
                <ImageSlider
                  //autoPlayWithInterval={3000}
                  images={props.item.imagesList}
                  style={{backgroundColor: 'transparent'}}
                  customSlide={({index, item, style, width}) => (
                    // It's important to put style here because it's got offset inside

                    <View style={{borderRadius: 10, marginHorizontal: 0}}>
                      <Image source={item} />
                    </View>
                  )}
                  customButtons={(position, move) => (
                    <View
                      style={[
                        styles.rowStyle,
                        {
                          position: 'absolute',
                          bottom: 15,
                          justifyContent: 'center',
                          width: '100%',
                        },
                      ]}>
                      {props.item.imagesList.map((image, index) => {
                        return (
                          <TouchableHighlight
                            key={index}
                            underlayColor="#ccc"
                            onPress={() => move(index)}
                            style={{
                              width: position === index ? 7 : 5,
                              height: position === index ? 7 : 5,
                              borderRadius: position === index ? 7 : 5,
                              backgroundColor:
                                position === index
                                  ? Colors.white
                                  : Colors.dotGrayColor,
                              marginHorizontal: 5,
                            }}>
                            <View />
                          </TouchableHighlight>
                        );
                      })}
                    </View>
                  )}
                />
              </View>
            )}
            <View
              style={{
                width: '100%',
                height: 1,
                backgroundColor: Colors.bottomLineColor,
                marginTop: 5,
              }}
            />
            <View style={[styles.rowStyle, {height: 40, alignItems: 'center'}]}>
              <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}>
                <Image source={images.upArrow} />
                <TextView
                  style={{
                    color: Colors.btnEnableColor,
                    marginStart: 2,
                    fontFamily: 'Inter-Bold',
                  }}>
                  {props.item.likeCount}
                </TextView>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}>
                <Image source={images.downArrow} />
                <TextView>{props.item.dislikeCount}</TextView>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}>
                <Image source={images.share} />
              </TouchableOpacity>
              <View
                style={{
                  height: 30,
                  width: 1,
                  backgroundColor: Colors.bottomLineColor,
                  alignSelf: 'center',
                }}></View>

              <TouchableOpacity
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <TextView
                  style={{
                    fontFamily: 'Inter-Bold',
                    fontSize: 14,
                    color: Colors.btnEnableColor,
                  }}>
                  {strings.DashBoard.reply}
                </TextView>
              </TouchableOpacity>
            </View>
            {props.item.replyCount !== 0 && (
              <TextView
                style={{
                  fontFamily: 'Inter-Thin',
                  fontSize: 12,
                  color: Colors.btnEnableColor,
                }}>
                {`${props.item.replyCount} replies`}
              </TextView>
            )}
          </View>
          {props.item.isTopAnswer && (
            <View
            style={{
              position: 'absolute',
              right: 30,
              top: -10,
              padding: 4,
              backgroundColor: Colors.btnEnableColor,
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              borderBottomLeftRadius: 15,
            }}>
            <TextView style={{fontFamily: 'Inter-Regular', fontSize: 6, color: Colors.white}}>{strings.DashBoard.topAnswer.toUpperCase()}</TextView>
          </View>
          )} 
        </View>
      ) : (
        <View style={styles.commentRootStyle}>
          <View
            style={[
              styles.commentViewStyle,
              {backgroundColor: Colors.selfMessageBgColor},
            ]}>
            <TextView
              style={{
                fontFamily: 'Inter-Regular',
                fontSize: 12,
                paddingVertical: 5,
              }}>
              {props.item.comment}
            </TextView>
            <View
              style={{
                width: '100%',
                height: 0.5,
                backgroundColor: Colors.bottomLineColor,
              }}/>

              <View style={[styles.rowStyle, {height: 40, alignItems: 'center'}]}>
                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}>
                  <Image source={images.upArrow} />
                  <TextView style={{fontFamily: 'Inter-Bold', color: Colors.btnEnableColor, marginStart: 2}}>{props.item.likeCount}</TextView>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}>
                  <Image source={images.downArrow} />
                  <TextView>{props.item.dislikeCount}</TextView>
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}>
                  <Image source={images.share} />
                </TouchableOpacity>

                <TouchableOpacity style={styles.rowStyle}>
                  <Image source={images.option} style={{
                            transform: [{ rotate: '90deg'}]}} />
                </TouchableOpacity>
              </View>
            
          </View>
          <Image
            source={{uri: props.item.userImg}}
            style={[styles.commentCircularImg,{ marginStart: 5}]}
          /> 
        </View>
      )}
    </View>
  );
};
export default CommentItemRow;
