import React, {useEffect, useState} from 'react';
import {View, FlatList, Image, TouchableOpacity,TextInput} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import StatusBar from '../../../utilities/StatusBar';
import ListItem from '../Dashbard/ListItem';
import images from '../../../utilities/images';
import Colors from '../../../utilities/Colors';
import TextView from '../../../utilities/TextView';
import CommentItemRow from './CommentItemRow';
import SharePost from '../Dashbard/sharePost/SharePost';
import ReportPost from '../Dashbard/ReportPost/index';
import styles from './styles';
import Strings from '../../../utilities/Strings';

const FeedDetails = (props) => {
  
  const [isShareDialogVisible, setShareDialogVisible] = useState(false);
  const [isReportDialogVisible, setReportDialogVisible] = useState(false);
  //const item=  props.route.params.item|| null;
  const item = useSelector(state => state.FeedReducer.selectFeed)
  const commentList = useSelector(state=> state.FeedReducer.commentList);
  
  useEffect(()=>{
      console.log('useEffect item ---> ', item)
  },[]);
    
  goBack=()=>{
      props.navigation.goBack();
  }


  renderHeader = () => (
    <View style={styles.headerStyle}>
      
      <TextView style={styles.headerTitleStyle}>{item.type}</TextView>
      <TouchableOpacity 
      style={styles.backButtonStyle}
      onPress={goBack}
      hitSlop={styles.backBtnHipSlop}>
        <Image source={images.backArrow} />
      </TouchableOpacity>
    </View>
  );

  shareBtnClick = (item) => {
    console.log('item clickl ', item);
    setShareDialogVisible(!isShareDialogVisible);
  };

  onReportBtnClick = (item) => {
    setReportDialogVisible(true);
  };
  sharePostVisibility = (isVisi) => {
    setShareDialogVisible(isVisi);
  };

  commentHeaderList=()=>(
    <ListItem
    item={item}
    isFromDetailPage={true}
    shareBtnClick={item =>shareBtnClick(item)}
    onReportClick={(item) => onReportBtnClick(item)}
    navigation={props.navigation}
  />
  )

  renderItem=({item})=>(
    <CommentItemRow item={item} />
  )

  return (
    <View style={styles.container}>
      {renderHeader()}
     
      <FlatList 
        data={commentList}
        contentContainerStyle={styles.commentListStyle}
        ListHeaderComponent={commentHeaderList}
        renderItem={renderItem}
      />
      <View  style={styles.bottomCommentViewStyle}>
        <View style={styles.grayBgViewStyle}>
          <TextInput style={styles.textInputStyle} multiline placeholder={Strings.DashBoard.commentPlaceHolder}/>
          <TextView style={styles.posyTxtStyle}>{Strings.DashBoard.Post}</TextView>
        </View>
      </View>
      <SharePost
        visibility={isShareDialogVisible}
        handleVisibility={(isVis) => sharePostVisibility(isVis)}
      />
      <ReportPost
        visibility={isReportDialogVisible}
        handleVisibility={(isVis) => setReportDialogVisible(isVis)}
      />
    </View>
  );
};

export default FeedDetails;
