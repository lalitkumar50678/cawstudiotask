import {StyleSheet, } from 'react-native';
import Colors from '../../../utilities/Colors';

const styles = StyleSheet.create({
  container:{
      flex: 1,
      color: Colors.white
  },
  commentRootStyle: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginTop: 10,
    flex : 1,
  },
  commentCircularImg: {width: 25, height: 25, borderRadius: 25},
  commentViewStyle: {
    marginStart: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.bottomLineColor,
    paddingHorizontal: 5,
    flex: 1
  },
  commentListNaveViewSty: {justifyContent: 'space-between', flexDirection: 'row',flex : 1},
  userNameTxtStyle: {
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    color: Colors.userNameColor,
  },
  userNameTimeStyle: {
    fontFamily: 'Inter-Thin',
    fontSize: 10,
    color: Colors.userNameColor,
    marginStart: 10
  },
  rowStyle: {flexDirection: 'row'},
  designation: {borderRadius: 5, backgroundColor: Colors.lightBlue, flexDirection: 'row', alignItems: 'center'},
  designationTxtStyle: {
    fontFamily: 'Inter-Bold',
    fontSize: 12,
    color: Colors.btnEnableColor,
  },
  lineStyle: {
    height: 10,
    width: 0.5,
    backgroundColor: Colors.bottomLineColor,
  },
  designationWithBold: {
    fontFamily: 'Inter-Bold',
    fontSize: 12,
    color: Colors.btnEnableColor,
    marginStart: 15,
  },
  experienceTxtStyle: {
    fontFamily: 'Inter-Thin',
    fontSize: 12,
    color: Colors.btnEnableColor,
    marginStart: 15,
  },
  headerStyle: {height: 45,justifyContent: 'center', alignItems: 'center', flexDirection: 'row', backgroundColor: Colors.white},
  commentListStyle: {paddingBottom: 70, backgroundColor: Colors.white},
  bottomCommentViewStyle: {position: 'absolute', bottom: 0,paddingVertical: 10,backgroundColor: Colors.white, width: '100%', justifyContent: 'center', paddingHorizontal: 20 },
  grayBgViewStyle: {flexDirection: 'row', backgroundColor: Colors.lightGray, borderRadius: 15, alignItems: 'center', justifyContent: 'flex-end'},
  textInputStyle: {flex: 1,},
  posyTxtStyle: {fontFamily: 'Inter-Bold', fontSize: 12, color: Colors.btnEnableColor, marginEnd: 10},
  headerTitleStyle: {fontSize: 16, fontFamily: 'Inter-Bold',textTransform: 'capitalize'},
  backButtonStyle: {position: 'absolute', start: 10, },
  backBtnHipSlop: {top: 15, right: 15, bottom: 15},
});

export default styles;
