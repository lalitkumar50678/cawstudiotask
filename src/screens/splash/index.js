import React,{useEffect} from 'react';
import { useDispatch, useSelector, connect } from "react-redux";
import {View, StyleSheet, Image, TextInput, TouchableOpacity} from 'react-native';
import Toast from 'react-native-simple-toast';
import commonStyle from '../../utilities/CommonStyle';
import StatusBar from '../../utilities/StatusBar';
import images from '../../utilities/images';
import Colors from '../../utilities/Colors';
import TextView from '../../utilities/TextView';
import Strings from '../../utilities/Strings';
import styles from './styles';
import {getUserLoginAction } from '../../redux/actions/LoginAction'
import { isUserLogin } from '../../utilities/AppData';

const SplashScreen = (props) => {

   
    const  dispatch = useDispatch();

    useEffect(()=>{
        isUserLogin(isLogin=>{
            console.log('getUserLoginAction calling ', isLogin);
            dispatch(getUserLoginAction(isLogin));  
             if(isLogin){
                 props.navigation.navigate("HomeScreen")
             }else {
                props.navigation.navigate("AuthScreen")
             }
        })
        

    },[])



  return (
    <View style={[commonStyle.container,styles.centerAlign]}>
      <StatusBar />
      <View style={styles.topViewStyle}>
        <Image source={images.signInLogo} style={styles.logoStyle} />
        <TextView style={styles.headerTitle}>
          {Strings.Login.welcomeTxt}
        </TextView>
      </View>
      
    </View>
  );
};


export default  SplashScreen;


