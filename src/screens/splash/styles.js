import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    logoStyle: {
      borderRadius: 5,
    },
    topViewStyle: {
      marginStart: 15,
      alignItems: 'center'
    },
    centerAlign: {justifyContent: 'center', alignItems: 'center'},
    headerTitle: {
      fontSize: 30,
      fontFamily: 'Inter-Bold',
    },
  });

  export default styles;