
import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {IS_USER_LOGIN} from './Constantas';

export const setUserLogin=(isLogin,callBack)=>{
    AsyncStorage.setItem(IS_USER_LOGIN,JSON.stringify(isLogin),callBack);
}


export const isUserLogin=(callBack)=>{
    AsyncStorage.getItem(IS_USER_LOGIN,(err, result) =>{
        console.log('is  user log ', result, 'Errro ', err);
        if(!err)
        callBack(result)
        else 
        callBack(err)
    });
}
