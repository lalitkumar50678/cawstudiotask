import React from 'react';

export default Object.freeze({
    yellow:'#f18926',
   
    gray: '#EBEDF2',
   
   
    white: '#ffffff',
   
    red: 'rgb(255,0,0)',
    lightGray: '#f6f9FC',
    blue : '#40469e',
    lightWhite: '#E5E5E5',
    flagFirstColor: '#FFA44A',
    flagSecondColor: '#ffffff',
    flagThirdColor: '#1A9F0B',
    flagCircleColor: '#181A93',
    phoneBottomColor: '#A5B1C2',
    textColor: '#A5B1C2',
    buttonBgColor: '#E8EBEF',
    btnEnableColor: '#00A981',
    txtGrayColor: '#A5B1C2',
    bottomLineColor: '#E8EBEF',
    lightBlue: '#E4F7F5',
    searchBox: '#CCEEE6',
    userNameColor: '#545B63',
    dotGrayColor : '#c9B3A8',
    selfMessageBgColor: '#F2F2F2',
    



    

})