import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from './Colors';

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: Colors.white
    }
})

export default styles;