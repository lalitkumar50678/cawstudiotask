import React from 'react';
import {StatusBar} from 'react-native';

const Statusbar=(props)=>{
    return(
        <StatusBar hidden={false}  barStyle={'dark-content'} translucent={false} />
    )
}
export default Statusbar;