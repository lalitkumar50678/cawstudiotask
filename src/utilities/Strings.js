import React from 'react';

export default Object.freeze({
    DashBoard:{
        Activity:'Activity',
        VideoShortcut: 'Video Shortcut',
        VideoCall: 'Video Call',
        Conferencing: 'Conferencing',
        Review: 'Review',
        Comment: 'Comment',
        Messaging: 'Messaging',
        CreateaPost:'Create a new post',
        community: 'community',
        allCommunities: 'All Communities',
        searchPlace: 'Search posts and members',
        readArticle: 'Read Article',
        sharePost: 'Share Post',
        youAnd: 'you and ',
        reply: 'Reply',
        topAnswer: 'Top Answer',
        Post: 'Post',
        commentPlaceHolder: 'Add a cheerful comment...',
    },
    Login:{
        welcomeTxt: 'Welcome To \nHealthNest',
        enterTxt: 'Enter Mobile Number',
        securityTxt: 'We never compromise on security! \n'
        +'Help us create a safe place by providing your mobile number to maintain authenticity.',
        sendOTP: 'Send OTP',
        correctPhoneNumber: 'Please enter correct phone number',
    }
})