import React from 'react';
import {Text, StyleSheet} from 'react-native';

const TextView = (props) => {
  return (
    <Text style={[styles.textstyle, props.style]} {...props}>
      {!!props.maxLength
        ? props.children.length > props.maxLength
          ? <Text>
            <Text>{`${props.children.toString(0, props.maxLength)}`}</Text>
            <Text style={styles.seeMoreSty}>{'...see more'}</Text>
          </Text>
          : `${props.children}`
        : `${props.children}`}
    </Text>
  );
};
export default TextView;
const styles = StyleSheet.create({
  textstyle: {
    fontSize: 12,
    fontFamily: 'Inter-Bold',
  },
  seeMoreSty:{
    fontFamily: 'Inter-Bold',
  }
});
