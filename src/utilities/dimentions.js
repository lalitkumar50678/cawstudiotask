import React from 'react';
import {Dimensions} from 'react-native';


export  const getWidth=(per)=>{
    const totalWidth = Dimensions.get('window').width;
    return (totalWidth*per)/100
}
 
export const getHeight=(per)=>{
    const totalHeight = Dimensions.get('window').height;
    return (totalHeight*per)/100
}