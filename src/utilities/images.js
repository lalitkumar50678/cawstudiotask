import React from 'react';
import signInLogo from '../../assets/login/logo.png';
import arrowdown from '../../assets/login/arrowdown.png';
import Shape from '../../assets/login/Shape.png';
import library from '../../assets/home/library.png';
import plus from '../../assets/home/plus.png';
import messages from '../../assets/home/messages.png';
import services from '../../assets/home/services.png';
import edit from '../../assets/feed/edit.png';
import poll from '../../assets/feed/poll.png';
import calender from '../../assets/feed/calender.png';
import question from '../../assets/feed/question.png';
import arrowLeft from '../../assets/home/arrowLeft.png';
import cross from '../../assets/home/cross.png';
import dummyImg from '../../assets/home/dumyImg.png';
import search from '../../assets/home/search.png';
import notification from '../../assets/home/notification.png'
import option from '../../assets/home/option.png';
import location from '../../assets/home/location.png';
import likedImg from '../../assets/home/likeImg.png';
import commentIng from '../../assets/home/commentImg.png';
import tags from '../../assets/home/tags.png';
import share from '../../assets/home/share.png';
import hand from '../../assets/home/hand.png';
import people from '../../assets/home/people.png';
import running from '../../assets/home/running.png';
import expert from '../../assets/home/expert.png';
import anonymous from '../../assets/home/anonymous.png';
import sendMsg from '../../assets/home/sendMsg.png';
import sharePost from '../../assets/home/sharePost.png'
import facebookIcon from '../../assets/home/facebookIcon.png';
import whatsApp from '../../assets/home/whatsApp.png';
import trigger from '../../assets/home/Trigger.png';
import feed from '../../assets/home/feed.png';
import eyes from '../../assets/home/eyes.png';
import unfollow from '../../assets/home/unfollow.png';
import report from '../../assets/home/report.png';
import attachment from '../../assets/home/attachment.png';
import thumb from '../../assets/home/thumb.png';
import haha from '../../assets/home/haha.png';
import sad from '../../assets/home/sad.png';
import backArrow from '../../assets/home/backArrow.png';
import influencer from '../../assets/home/infuencer.png';
import upArrow from '../../assets/home/upArrow.png';
import downArrow from '../../assets/home/Downvote.png';
import images from '../../assets/home/image.png';


export default {
    signInLogo,
    arrowdown,
    Shape,
    library,
    plus,
    messages,
    services,
    edit,
    poll,
    calender,
    question,
    arrowLeft,
    cross,
    dummyImg,
    search,
    notification,
    option,
    location,
    likedImg,
    commentIng,
    tags,
    share,
    hand,
    people,
    running,
    expert,
    anonymous,
    sendMsg,
    sharePost,
    facebookIcon,
    whatsApp,
    trigger,
    feed,
    unfollow,
    eyes,
    report,
    attachment,
    thumb,
    haha,
    sad,
    backArrow,
    influencer,
    upArrow,
    downArrow,
    images

    
}

